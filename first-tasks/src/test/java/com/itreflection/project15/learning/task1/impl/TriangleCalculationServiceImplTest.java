package com.itreflection.project15.learning.task1.impl;

import static org.slf4j.LoggerFactory.getLogger;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

@RunWith(MockitoJUnitRunner.class)
public class TriangleCalculationServiceImplTest {

  private final Logger logger = getLogger(TriangleCalculationServiceImplTest.class);

  @InjectMocks
  private TriangleCalculationServiceImpl triangleCalculationService;

}